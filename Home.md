# SML CodeStyle

В данном репозитории собраны стили оформления кода по разным языкам, используемым в компании при разарботке.

- [Objective-C](Objective-C.md)
- [Java](Java.md)
- [JavaScript](http://contribute.jquery.org/style-guide/js/)
- [PHP](http://framework.zend.com/manual/1.12/ru/coding-standard.coding-style.html)

Если в проекте используется нестандартный CodeStyle, то в корне проекта **обязательно** должен находиться `Contribute.md`, в котором описан CodeStyle проекта.