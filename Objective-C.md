# Objective-C CodeStyle

## Организация (группировка) кода

Используйте `#pragma mark -` чтобы разделять методы в группы по их назначению, в соответствии со следующей общей структурой:

```objc
#pragma mark - Lifecycle

- (instancetype)init {}
- (void)dealloc {}
- (void)viewDidLoad {}
- (void)viewWillAppear:(BOOL)animated {}
- (void)didReceiveMemoryWarning {}

#pragma mark - Custom Accessors

- (void)setCustomProperty:(id)value {}
- (id)customProperty {}

#pragma mark - IBActions

- (IBAction)submitData:(id)sender {}

#pragma mark - Public

- (void)publicMethod {}

#pragma mark - Private

- (void)privateMethod {}

#pragma mark - Protocol conformance
#pragma mark - UITextFieldDelegate
#pragma mark - UITableViewDataSource
#pragma mark - UITableViewDelegate

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {}

#pragma mark - NSObject

- (NSString *)description {}
```

## Пробелы

- Для отступов использовать **только** пробелы, ширина отступа 4 пробела.
- Фигурные скобки открываются и закрываются в отдельной строке.

```objc
if (a > b)
{
	c = a;
}
```

- Реализация методов разделяется **ровно** одной пустой строкой.
- В методах пустая строка используется для группировки кода по смыслу (лучше выделять эти блоки в отдельные методы).
- Авто синтезация предпочтительнее, но при необходимости можно использовать `@synthesize` и `@dynamic`
- Не использовать выравнивание по двоеточию внутри метода.
- Использовать один пробел после `,` и опускать пробелы до `,`
- Использовать один пробел с внешней стороны `()` и опускать пробелы внутри `()`, кроме определения методов, где пробелы вне скобок не ставятся.
- Отделять *звездочку* следующим образом: `NSString *name`

```objc
@property (strong, nonatomic) NSString *name;
```

- Отделять операторы одним пробелом: `a = a + ((1 - b) / c)`. *Исключение: унарные операторы (`!`, `++`...) пробелом не отделяются.*
- `if/else/for/while/try` всегда должны иметь скобки. Однострочный вариант не допускается.

## Именование

Использовать длинные описательные имена. Например `settingsButton` вместо `setBut`.
Использовать двухбуквенный префикс для классов и констант.
Константы должны быть в CamelCase со всеми словами с большой буквы, с префиксом из имени класса, к которому они относятся, например:

```objc
static NSTimeInterval const RSTabViewControllerNavigationFadeAnimationDuration = 0.3;
```

## Методы

Объявление метода должно содержать пробел после типа, а так-же между частями метода. 

```objc
+ (instancetype)generateGraphWithName:(NSString *)name vertexWeights:(NSArray *)vertexWeights andEdgesWeights:(NSArray *)edgesWeights;
```

## Переменные

Названия переменных должны быть настолько описательными, насколько возможно (исключения составляют индексные переменные в `for`).

В интерфейсной части класса **всегда** давать предпочтение свойствам, а не полям класса. Поля в интерфейсной части **недопустимы**.

Так-же предпочитать приватные свойства полям класа. При необходимости использования поля, его имя должно начинаться с подчеркивания и идти в camelCase, например: `NSString *_firstName`, но автоматическая синтезация более предпочтительна.
Локальные переменные не должны содержать подчеркиваний.

Обращаться напрямую к полям класса разрешается **только** в инициализационных методах, `dealloc` и кастомных сеттерах/геттерах.

Аттрибуты свойств должны быть обязательно перечислены в порядке: storage, atomicity

## Литералы

Литералы предпочтительнее при создании немутирующих объектов:  

**Предпочтительнее:**

```objc
NSArray *names = @[@"Brian", @"Matt", @"Chris", @"Alex", @"Steve", @"Paul"];
NSDictionary *productManagers = @{@"iPhone": @"Kate", @"iPad": @"Kamal", @"Mobile Web": @"Bill"};
NSNumber *shouldUseLiterals = @YES;
NSNumber *buildingStreetNumber = @10018;
```

**Хуже:**

```objc
NSArray *names = [NSArray arrayWithObjects:@"Brian", @"Matt", @"Chris", @"Alex", @"Steve", @"Paul", nil];
NSDictionary *productManagers = [NSDictionary dictionaryWithObjectsAndKeys: @"Kate", @"iPhone", @"Kamal", @"iPad", @"Bill", @"Mobile Web", nil];
NSNumber *shouldUseLiterals = [NSNumber numberWithBool:YES];
NSNumber *buildingStreetNumber = [NSNumber numberWithInteger:10018];
```

## Магические числа

**Никогда!** не писать в коде конструкций типа:

```objc
if (a < 0.000001) 
{...}
``` 
В данном случае `0.000001` признается магическим числом и должно быть исключено. Если у Вас появляется необходимость использовать здесь какое-то число, подумайте: 

1. Быть может, есть такая определенная константа, если ее нет, то возможно ее стоит определить. 
2. Быть может, стоит передавать это значение как параметр метода. 
3. Быть может, стоит хранить это значение как член класса. 
4. Быть может, есть еще какие-нибудь выходы. 
  
Как бы там ни было, но **магических чисел** в коде **быть не должно!** К таковым относятся и «магические строки».